using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager GameInstance;

    [SerializeField]
    private GameObject[] _Players;

    private int _PlayerIndex;

    public GameManager()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public int PlayerIndex
    {
        get
        {
            return this._PlayerIndex;
        }
        set
        {
            this._PlayerIndex = value;
        }
    }

    private void Awake()
    {
        if (GameInstance == null)
        {
            GameInstance = this;

            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += this.OnLevelFinishedLoading;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= this.OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode sceneMode)
    {
        if(scene.name == "GamePlay")
        {
            Instantiate(this._Players[this.PlayerIndex]);
        }
    }
} // class GameManager
