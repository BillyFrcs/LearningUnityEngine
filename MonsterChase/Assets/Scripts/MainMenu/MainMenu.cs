using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour
{
    public MainMenu()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void PlayGame()
    {
        // Load scene to go to GamePlay
        int selectedPlayer = int.Parse(EventSystem.current.currentSelectedGameObject.name);
 
        GameManager.GameInstance.PlayerIndex = selectedPlayer;

        SceneManager.LoadScene("GamePlay");

        //  Debug.Log("Play Game"); // DEBUG
    }
} // class MainMenu