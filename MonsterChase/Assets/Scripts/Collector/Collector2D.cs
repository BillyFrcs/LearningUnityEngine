using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector2D : MonoBehaviour
{
    private string _EnemyTag;
    private string _PlayerTag;

    public Collector2D()
    {
        this._EnemyTag = "Enemy";
        this._PlayerTag = "Player";
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D Collision)
    {
        // Destroy game object if it's collider with left and right sides
        if (Collision.CompareTag(this._EnemyTag) || Collision.CompareTag(this._PlayerTag))
        {
            Destroy(Collision.gameObject);
        }
    }
} // class Collector2D
