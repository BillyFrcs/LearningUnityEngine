using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Enemies_References;

    [SerializeField]
    private Transform Left_Position;

    [SerializeField]
    private Transform Right_Position;

    private GameObject Spawn_Enemies;

    private int _RandomIndex;
    private int _RandomSide;

    public EnemiesSpawner()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        this.StartCoroutine(this.SpawnEnemies());
    }

    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator SpawnEnemies()
    {
        // Spawn random enemies
        while (true)
        {
            const int timeStart = 1;
            const int timeEnd = 5;

            // Enemies spawn for 5 seconds
            yield return new WaitForSeconds(Random.Range(timeStart, timeEnd));

            const int minTime = 0;
            const int maxTime = 2;

            this._RandomIndex = Random.Range(minTime, this.Enemies_References.Length);

            this._RandomSide = Random.Range(minTime, maxTime);

            this.Spawn_Enemies = Instantiate(this.Enemies_References[this._RandomIndex]);

            const int randomLeft = 4;
            const int randomRight = 10;

            if (this._RandomSide == 0) // Spawn from the left side
            {
                this.Spawn_Enemies.transform.position = this.Left_Position.position;

                this.Spawn_Enemies.GetComponent<Enemies>().EnemiesSpeed = Random.Range(randomLeft, randomRight);
            }
            else // Spawn from the right side
            {
                this.Spawn_Enemies.transform.position = this.Right_Position.position;

                this.Spawn_Enemies.GetComponent<Enemies>().EnemiesSpeed = -Random.Range(randomLeft, randomRight);

                // Flip the enemies
                const float x = -1f;
                const float y = 1f;
                const float z = 1f;

                this.Spawn_Enemies.transform.localScale = new Vector3(x, y, z);
            }
        }
    }
} // class EnemiesSpawner
