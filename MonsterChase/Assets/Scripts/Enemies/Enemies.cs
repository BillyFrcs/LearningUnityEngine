using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    [HideInInspector]
    public float EnemiesSpeed;

    private Rigidbody2D Enemies_RB2D;

    public Enemies()
    {
    }

    private void Awake()
    {
        this.Enemies_RB2D = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    void FixedUpdate()
    {
        this.Enemies_RB2D.velocity = new Vector2(this.EnemiesSpeed, this.Enemies_RB2D.velocity.y);
    }
} // class Enemies
