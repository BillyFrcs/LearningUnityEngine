using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraFollow : MonoBehaviour
{
    private Transform Player_Transform;
    private Vector3 Transform_Position;

    [SerializeField]
    private float _MinX;

    [SerializeField]
    private float _MaxX;

    public PlayerCameraFollow()
    {
        this._MinX = 0.0f;
        this._MaxX = 0.0f;
    }

    // Start is called before the first frame update
    void Start()
    {
        this.Player_Transform = GameObject.FindWithTag("Player").transform;

        // Debug.Log("Selected player index: " + GameManager.GameInstance.PlayerIndex); // DEBUG
    }

    // Update is called once per frame
    void Update()
    {
    }

    void LateUpdate()
    {
        if (!this.Player_Transform)
            return;

        this.Transform_Position = transform.position;
        this.Transform_Position.x = Player_Transform.position.x;

        if (this.Transform_Position.x < this._MinX)
        {
            this.Transform_Position.x = this._MinX;
        }

        if (this.Transform_Position.x > this._MaxX)
        {
            this.Transform_Position.x = this._MaxX;
        }

        transform.position = this.Transform_Position;
    }
} // class PlayerCameraFollow
