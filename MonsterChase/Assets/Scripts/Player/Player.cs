using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Using this function to visible the object in Unity inspector
    [SerializeField]
    private float _PlayerMovement;

    [SerializeField]
    private float _PlayerJumpForce;

    private float _MovementX;

    private string _WalkAnimation;
    private string _JumpAnimation;
    private string _GroundTag;
    private string _EnemyTag;

    private bool _IsGrounded;

    // Unity objects
    private Rigidbody2D Player_RB2D;
    private Animator Player_Animator;
    private SpriteRenderer Player_SR;

    public Player()
    {
        this._PlayerMovement = 5.0f;
        this._PlayerJumpForce = 10.0f;

        // Animation
        this._WalkAnimation = ("Walk");
        this._JumpAnimation = ("Jump");
        this._GroundTag = ("Ground");
        this._EnemyTag = ("Enemy");

        this._IsGrounded = true;
    }

    private void Awake()
    {
        // Get the player components
        this.Player_RB2D = GetComponent<Rigidbody2D>();
        this.Player_Animator = GetComponent<Animator>();
        this.Player_SR = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // Update player move
        this.PlayerMove();

        // Update player move animation
        this.PlayerMoveAnimation();
    }

    private void FixedUpdate()
    {
        // Fixed update player jump
        this.PlayerJump();
    }

    // Move player using keyboard
    void PlayerMove()
    {
        // Get the player movement
        List<string> player = new List<string>();
        player.Add("Horizontal");

        this._MovementX = Input.GetAxisRaw(player[0]);

        const float movementY = 0.0f;
        const float movementZ = 0.0f;

        // Move the player
        // transform.position += new Vector3(this._MovementX, movementY, movementZ) * this._PlayerMovement * Time.deltaTime;
        transform.position += new Vector3(this._MovementX, movementY, movementZ) * this._PlayerMovement * Time.deltaTime;

        // Debug.Log("Player move: " + this._MovementX); // DEBUG
    }

    // Player animation
    void PlayerMoveAnimation()
    {
        const float maxMovement = 0.0f;

        // Player walk animation
        if (this._MovementX > maxMovement) // Move player to the right side
        {
            this.Player_Animator.SetBool(this._WalkAnimation, true);

            const bool isFlipX = false;

            this.Player_SR.flipX = isFlipX;
        }
        else if (this._MovementX < maxMovement) // Move player to the left side
        {
            this.Player_Animator.SetBool(this._WalkAnimation, true);

            const bool isFlipX = true;

            this.Player_SR.flipX = isFlipX;
        }
        else // Player not move
        {
            this.Player_Animator.SetBool(this._WalkAnimation, false);
        }
    }

    // Player jump
    void PlayerJump()
    {
        const float xJumpForce = 0.0f;

        if (Input.GetButtonUp(this._JumpAnimation) && this._IsGrounded)
        {
            this._IsGrounded = false;

            this.Player_RB2D.AddForce(new Vector2(xJumpForce, this._PlayerJumpForce), ForceMode2D.Impulse);

            // Debug.Log("Jump pressed"); // DEBUG
        }
    }

    private void OnCollisionEnter2D(Collision2D Collision)
    {
        if (Collision.gameObject.CompareTag(this._GroundTag))
        {
            this._IsGrounded = true;

            // Debug.Log("Player landed"); // DEBUG
        }

        if (Collision.gameObject.CompareTag(this._EnemyTag))
        {
            // Destroy the player when get collided
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D Collider)
    {
        // Destroy the player when get trigger
        if (Collider.CompareTag(this._EnemyTag))
            Destroy(gameObject);
    }
} // class Player
