using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlayUI : MonoBehaviour
{
    public GamePlayUI()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Restart the game 
    public void RestartGame()
    {
        // Load the GamePlay scene when click the restart button
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

       // SceneManager.LoadScene("GamePlay");
    }

    // Back to home game
    public void HomeGame()
    {
        // Load the MainMenu scene when click the home button
        SceneManager.LoadScene("MainMenu");
    }
}
