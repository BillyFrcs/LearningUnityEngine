using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunWeaponController : WeaponsController
{
    public Transform SpawnPoint;
    public GameObject BulletPrefab;
    public ParticleSystem FXShoot;
    public GameObject FXBulletFall;

    private Collider2D Fire_Collider;
    private WaitForSeconds Wait_Time;
    private WaitForSeconds Wait_FireCollider;

    public GunWeaponController()
    {
        this.Wait_Time = new WaitForSeconds(0.02f);
        this.Wait_FireCollider = new WaitForSeconds(0.02f);
    }

    // Start is called before the first frame update
    void Start()
    {
        // Create bullets
    }

    // Update is called once per frame
    void Update()
    {
    }

    public override void ProcessAttack()
    {
        // Process attack with weapon
        switch (WeaponName)
        {
            case WeaponNames.Pistol:
                print("Pistol is shoot"); // DEBUG
                break;

            case WeaponNames.Mp5:
                print("Mp5 is shoot"); // DEBUG
                break;

            case WeaponNames.M3:
                print("Shotgun is shoot"); // DEBUG
                break;

            case WeaponNames.Ak:
                print("Ak47 is shoot"); // DEBUG
                break;

            case WeaponNames.Awp:
                print("Sniper is shoot"); // DEBUG
                break;

            case WeaponNames.Fire:
                print("Fire Gun is shoot"); // DEBUG
                break;

            case WeaponNames.Rocket:
                print("Rocket is shoot"); // DEBUG
                break;
        }

        // Spawn bullets
    }

    IEnumerator WaitForShootEffect()
    {
        yield return this.Wait_Time;

        // Play the shoot particle system
        this.FXShoot.Play();
    }

    IEnumerator ActiveFireCollider()
    {
        this.Fire_Collider.enabled = true;

        yield return this.Wait_FireCollider;

        this.Fire_Collider.enabled = false;
    }
} // class GunWeaponController
