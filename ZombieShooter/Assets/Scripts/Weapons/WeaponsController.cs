using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Weapon configuration
public enum TypeControlAttack
{
    Click,
    Hold
}

public enum TypeWeapon
{
    Melee,
    OneHand,
    TwoHand
}

[System.Serializable] // Make the default config are visible in the inspector panel
public struct DefaultConfiguration
{
    public TypeControlAttack TypeControlAttack;
    public TypeWeapon TypeWeapon;

    [Range(0, 100)]
    public int Damage;

    [Range(0, 100)]
    public int CriticalDamage;

    [Range(0, 100)]
    public int CriticalRate;

    [Range(0.1f, 1.0f)]
    public float FireRate;
}

public enum WeaponNames
{
    Melee,
    Pistol,
    Mp5,
    M3,
    Ak,
    Awp,
    Fire,
    Rocket
}

public class WeaponsController : MonoBehaviour
{
    public DefaultConfiguration DefaultConfig;
    public WeaponNames WeaponName;

    protected PlayerAnimation Player_Animation;
    protected float _LastShoot;

    public int GunIndex;
    public int CurrentBullet;
    public int MaxBullets;

    public WeaponsController()
    {
        CurrentBullet = MaxBullets;
    }

    void Awake()
    {
        this.Player_Animation = this.GetComponentInParent<PlayerAnimation>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    { 
    }

    public void CallAttack()
    {
        if(Time.time > (_LastShoot + DefaultConfig.FireRate))
        {
            const int emptyBullet = 0;

            if(CurrentBullet > emptyBullet) // If the weapon has bullets
            {
                // Update process attack
                this.ProcessAttack();

                // Animated player attack
                Player_Animation.PlayerAttackAnimation();

                _LastShoot = Time.time;

                // Decrease the bullets when player attack
                CurrentBullet--;
            }
            else
            {
                // Player no ammo sound
            }
        }
    }

    public virtual void ProcessAttack()
    {
    }
} // class WeaponsController