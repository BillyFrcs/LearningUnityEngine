using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsManager : MonoBehaviour
{
    public List<WeaponsController> WeaponUnlocked;
    public WeaponsController[] TotalWeapon;

    [HideInInspector]
    public WeaponsController CurrentWeapon;

    private int CurrentWeaponIndex;

    private TypeControlAttack CurrentType_Control;

    private PlayerArmController[] Arm_Controller;
    private PlayerAnimation Player_Animation;

    private bool _IsShooting;

    void Awake()
    {
        this.Player_Animation = this.GetComponent<PlayerAnimation>();

        // Load active weapons
        this.LoadActiveWeapons();
    }

    public WeaponsManager()
    {
        CurrentWeaponIndex = 1;
    }

    // Start is called before the first frame update
    void Start()
    {
        this.Arm_Controller = this.GetComponentsInChildren<PlayerArmController>();

        // Change weapon
        const int startIndex = 1;

        this.ChangeWeapon(this.WeaponUnlocked[startIndex]);

        this.Player_Animation.PlayerSwitchWeaponAnimation
        (
            (int)this.WeaponUnlocked[CurrentWeaponIndex].DefaultConfig.TypeWeapon
        );
    }

    // Update is called once per frame
    void Update()
    {
    }

    void LoadActiveWeapons()
    {
        const int index = 0;

        this.WeaponUnlocked.Add(TotalWeapon[index]);

        for (uint weapon = 1; weapon < this.TotalWeapon.Length; weapon++)
        {
            this.WeaponUnlocked.Add(TotalWeapon[weapon]);
        }
    }

    public void SwitchWeapon()
    {
        CurrentWeaponIndex++;

        const int index = 0;

        CurrentWeaponIndex = (CurrentWeaponIndex >= this.WeaponUnlocked.Count) ? index : CurrentWeaponIndex;

        this.Player_Animation.PlayerSwitchWeaponAnimation
        (
            (int)this.WeaponUnlocked[CurrentWeaponIndex].DefaultConfig.TypeWeapon
        );

        // Change weapon
        this.ChangeWeapon(this.WeaponUnlocked[CurrentWeaponIndex]);

        /* Another way to do this
        if(CurrentWeaponIndex >= this.WeaponUnlocked.Count)
        {
            CurrentWeaponIndex = index;
        }
        else
        {
            CurrentWeaponIndex = CurrentWeaponIndex;
        }
        */
    }

    void ChangeWeapon(WeaponsController NewWeaponController)
    {
        if (this.CurrentWeapon)
        {
            this.CurrentWeapon.gameObject.SetActive(false);
        }

        this.CurrentWeapon = NewWeaponController;

        this.CurrentType_Control = NewWeaponController.DefaultConfig.TypeControlAttack;

        NewWeaponController.gameObject.SetActive(true);

        if (NewWeaponController.DefaultConfig.TypeWeapon == TypeWeapon.TwoHand)
        {
            for (uint weapon = 0; weapon < this.Arm_Controller.Length; weapon++)
            {
                this.Arm_Controller[weapon].ChangeToTwoHand();
            }
        }
        else
        {
            for (uint weapon = 0; weapon < this.Arm_Controller.Length; weapon++)
            {
                this.Arm_Controller[weapon].ChangeToOneHand();
            }
        }
    }

    public void Attack()
    {
        if (this.CurrentType_Control == TypeControlAttack.Hold)
        {
            this.CurrentWeapon.CallAttack();
        }
        else if (this.CurrentType_Control == TypeControlAttack.Click)
        {
            if (!_IsShooting)
            {
                this.CurrentWeapon.CallAttack();

                const bool isShoot = true;

                _IsShooting = isShoot;
            }
        }
    }

    public void ResetAttack()
    {
        const bool isShoot = false;

        _IsShooting = isShoot;
    }
} // class WeaponsManager
