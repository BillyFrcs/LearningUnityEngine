using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public Animator PlayerAnimator;

    private List<string> _AnimationName = new List<string>();

    public PlayerAnimation()
    {
        // Add list animations name and parameters
        _AnimationName.Add("Run");
        _AnimationName.Add("Attack");
        _AnimationName.Add("TypeWeapon");
        _AnimationName.Add("Switch");
        _AnimationName.Add("Hurt");
        _AnimationName.Add("Dead");
    }

    void Awake()
    {
        this.PlayerAnimator = this.GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void PlayerMoveAnimation(bool IsRun)
    {
        this.PlayerAnimator.SetBool(_AnimationName[0], IsRun);
    }

    public void PlayerAttackAnimation()
    {
        this.PlayerAnimator.SetTrigger(_AnimationName[1]);
    }

    public void PlayerSwitchWeaponAnimation(int TypeWeapon)
    {
        // Type weapon
        this.PlayerAnimator.SetInteger(_AnimationName[2], TypeWeapon);

        // Switch weapon
        this.PlayerAnimator.SetTrigger(_AnimationName[3]);
    }

    public void PlayerHurtAnimation()
    {
        this.PlayerAnimator.SetTrigger(_AnimationName[4]);
    }

    public void PlayerDeadAnimation()
    {
        this.PlayerAnimator.SetTrigger(_AnimationName[5]);
    }
} // class PlayerAnimation
