using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArmController : MonoBehaviour
{
    public Sprite OneHandSprite;
    public Sprite TwoHandSprite;

    private SpriteRenderer Sprite_Render;

    public PlayerArmController()
    {
    }

    void Awake()
    {
        this.Sprite_Render = this.GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ChangeToOneHand()
    {
        this.Sprite_Render.sprite = this.OneHandSprite;
    }

    public void ChangeToTwoHand()
    {
        this.Sprite_Render.sprite = this.TwoHandSprite;
    }
} // class PlayerArmController
