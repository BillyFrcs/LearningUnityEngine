using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D Player_RB2D;
    private PlayerAnimation Player_Animation;

    private float _MoveForceX;
    private float _MoveForceY;

    // private float _PlayerMovementSpeed;

    public PlayerMovement()
    {
        this._MoveForceX = 1.5f;
        this._MoveForceY = 1.5f;

        // this._PlayerMovementSpeed = 5.0f;
    }

    void Awake()
    {
        this.Player_RB2D = this.GetComponent<Rigidbody2D>();
        this.Player_Animation = this.GetComponent <PlayerAnimation>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    void FixedUpdate()
    {
        // Update the player's movement
        this.PlayerMove();
    }

    void PlayerMove()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        // Move the player horizontally
        if (horizontal > (float)0f)
        {
            this.Player_RB2D.velocity = new Vector2(_MoveForceX, this.Player_RB2D.velocity.y);
        }
        else if (horizontal < (float)0f)
        {
            this.Player_RB2D.velocity = new Vector2(-_MoveForceX, this.Player_RB2D.velocity.y);
        }
        else
        {
            this.Player_RB2D.velocity = new Vector2(0f, this.Player_RB2D.velocity.y);
        }

        // Move the player vertically
        if (vertical > (float)0f)
        {
            this.Player_RB2D.velocity = new Vector2(this.Player_RB2D.velocity.x, _MoveForceY);
        }
        else if (vertical < (float)0f)
        {
            this.Player_RB2D.velocity = new Vector2(this.Player_RB2D.velocity.x, -_MoveForceY);
        }
        else
        {
            this.Player_RB2D.velocity = new Vector2(this.Player_RB2D.velocity.x, 0f);
        }

        // Update player animation
        this.UpdatePlayerAnimation();

        // Move player face scale when player move left or right
        Vector3 FaceScale = this.transform.localScale;

        if (horizontal > (float)0f)
        {
            FaceScale.x = -(float)1f;
        }
        else if(horizontal < (float)0f)
        {
            FaceScale.x = (float)1f;
        }

        this.transform.localScale = FaceScale;

        /*
        // Alternative player movement
        float Horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");

        Vector3 Position = transform.position;

        Position.x += Horizontal * this._PlayerMovementSpeed * Time.deltaTime;
        Position.y += Vertical * this._PlayerMovementSpeed * Time.deltaTime;

        transform.position = Position;
        */
    }

    void UpdatePlayerAnimation()
    {
        // Animate the player idle and velocity
        bool isRun;

        if(this.Player_RB2D.velocity.x != 0f || this.Player_RB2D.velocity.y != 0f)
        {
            isRun = true;

            this.Player_Animation.PlayerMoveAnimation(isRun); // Player run
        }
        else if(this.Player_RB2D.velocity.x == 0f && this.Player_RB2D.velocity.y == 0f)
        {
            isRun = false;

            this.Player_Animation.PlayerMoveAnimation(isRun); // Player idle
        }
    }
} // class PlayerMovement
