using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : MonoBehaviour
{
    private WeaponsManager Weapon_Manager;

    [HideInInspector]
    public bool IsCanShoot;

    private bool _IsHoldAttack;

    public PlayerInputController()
    {
        IsCanShoot = true;
    }

    void Awake()
    {
        this.Weapon_Manager = GetComponent<WeaponsManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // Update player controller
        this.PlayerController();
    }

    private void PlayerController()
    {
        // Switch weapon when press Q down
        if (Input.GetKeyDown(KeyCode.Q))
        {
            this.Weapon_Manager.SwitchWeapon();
        }

        // Player shoot
        if (Input.GetKeyDown(KeyCode.L))
        {
            const bool isHold = true;

            _IsHoldAttack = isHold;

            // Debug.Log("L key is pressed"); // DEBUG
        }
        else
        {
            this.Weapon_Manager.ResetAttack();

            const bool isHold = false;

            _IsHoldAttack = isHold;
        }

        // Player can shoot when the L key is hold
        if (_IsHoldAttack && IsCanShoot)
        {
            // Debug.Log("Player shooting"); // DEBUG

            this.Weapon_Manager.Attack();
        }
    }
} // class PlayerInputController
