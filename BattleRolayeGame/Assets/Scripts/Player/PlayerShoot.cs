using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    [Header("Camera")]
    public Camera CameraPlayer;

    [SerializeField]
    private PlayerController _PlayerController;

    [Header("Shoot")]
    [SerializeField]
    private float _damage;

    public bool isCanShoot;

    private List<string> _parameterAnimator = new List<string>();
    private WaitForSeconds _ShootDelay;

    public PlayerShoot()
    {
        _parameterAnimator.Add("Shoot");

        _damage = 10;

        isCanShoot = true;

        _ShootDelay = new WaitForSeconds(0.2f);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    void Shoot()
    {
        if(Input.GetMouseButtonDown(0)) // Left mouse button
        {
            _PlayerController.PlayerAnimator.SetBool(_parameterAnimator[0], true);
        }

        if(Input.GetMouseButtonUp(0)) // Left mouse button
        {
            _PlayerController.PlayerAnimator.SetBool(_parameterAnimator[0], false);
        }

        // Shoot every frame
        if(Input.GetMouseButton(0) && isCanShoot)
        {
            // Delay when shooting
            StartCoroutine(ShootDelayCoroutine());

            RaycastHit hit;

            if (Physics.Raycast(CameraPlayer.transform.position, CameraPlayer.transform.forward, out hit))
            {
                // Player shoot object
                if (hit.collider.GetComponent<Health>())
                {
                    Instantiate(VFXManager.Instance.BloodImpact, hit.point, Quaternion.identity);

                    hit.collider.GetComponent<Health>().TakeDamage(_damage);
                }
                else
                {
                    Instantiate(VFXManager.Instance.WoodImpact, hit.point, Quaternion.identity);
                }

                // Debug.Log("Shoot object: " + hit.collider.name); // DEBUG
            }
        }
    }

    private IEnumerator ShootDelayCoroutine()
    {
        isCanShoot = false;

        yield return _ShootDelay;

        isCanShoot = true;
    }
}
