using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    public CharacterController ControlCharacter;
    public float movementSpeed;

    [Header("Rotation")]
    private Vector2 _Rotation;
    public float rotationSpeed;

    [Header("Animator")]
    public Animator PlayerAnimator;

    public PlayerController()
    {
        movementSpeed = 5.0f;
        rotationSpeed = 0.0f;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Lock cursor when play game
        Cursor.visible = false;

        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        // Movement player
        MovementPlayer();

        // Rotation player
        RotationPlayer();
    }

    private void MovementPlayer()
    {
        float xMovement = Input.GetAxis("Horizontal");
        float zMovement = Input.GetAxis("Vertical");
        float yMovement = -1.0f;

        float moveAnimation = new Vector2(xMovement, zMovement).magnitude;
        PlayerAnimator.SetFloat("Movement", moveAnimation, 0.1f, Time.deltaTime);

        Vector3 movement = new Vector3(xMovement, yMovement, zMovement);

        movement = transform.TransformDirection(movement);

        if (xMovement == 0 && zMovement == 0)
            movement = Vector3.zero;

        ControlCharacter.Move(movement * movementSpeed * Time.deltaTime);
    }

    private void RotationPlayer()
    {
        // Rotate player at position x
        _Rotation.x += Input.GetAxis("Mouse X") * rotationSpeed;

        // Rotate player at position y
        _Rotation.y += Input.GetAxis("Mouse Y") * rotationSpeed;

        float min = -10f;
        float max = 10;

        _Rotation.y = Mathf.Clamp(_Rotation.y, min, max);

        transform.localRotation = Quaternion.Euler(_Rotation.y, _Rotation.x, 0.0f);
    }
}
