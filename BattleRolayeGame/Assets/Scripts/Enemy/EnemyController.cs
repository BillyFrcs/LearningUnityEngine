using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Health EnemyHealth;

    [Header("Animation")]
    public Animator EnemyAnimator;

    private CapsuleCollider _Collider;

    private WaitForSeconds _DeadDelay;

    public bool isDead;

    public EnemyController()
    {
        isDead = false;

        _DeadDelay = new WaitForSeconds(0.9f);
    }

    private void Awake()
    {
        _Collider = GetComponent<CapsuleCollider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        EnemyHealth.Dead += Dead;
    }

    // Update is called once per frame
    void Update()
    {
   
    }

    void Dead()
    {
        _Collider.enabled = false;

        EnemyAnimator.SetTrigger("Dead");

        StartCoroutine(DeadDelayCoroutine());
    }

    private IEnumerator DeadDelayCoroutine()
    {
        isDead = true;

        yield return _DeadDelay;

        if (_DeadDelay != null)
            Destroy(gameObject);

        isDead = false;
    }
}
