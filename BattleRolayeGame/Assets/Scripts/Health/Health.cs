using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Health : MonoBehaviour
{
    [SerializeField]
    private float _health;

    public event Action Dead;

    private bool _isDead;

    public Health()
    {
        _health = 100;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float Hp
    {
        get
        {
            return _health;
        }
        set
        {
            _health = value;

            if (_health <= 0)
            {
                _isDead = true;

                Dead?.Invoke();
            }

            // print("Reduce HP"); // DEBUG
        }
    }

    public void TakeDamage(float damageAmount)
    {
        if (_isDead)
            return;

        Hp -= damageAmount;
    }
}
